package br.com.kamila.Teste.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.kamila.Teste.model.Estado;
import br.com.kamila.Teste.repository.EstadoRepository;

@Service
public class EstadoService {

	@Autowired
	EstadoRepository estadoRepository;
	
	public Estado save(Estado estado) {
		return estadoRepository.save(estado);
	}
}
