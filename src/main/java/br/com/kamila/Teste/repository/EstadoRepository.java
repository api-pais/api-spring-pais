package br.com.kamila.Teste.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.kamila.Teste.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Long>{

}
